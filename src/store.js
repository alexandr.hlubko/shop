import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
const productlist = localStorage.getItem("productlist") ? JSON.parse(localStorage.getItem("productlist")) : []; // Достаем из localStorage список добавленных товаров
export default new Vuex.Store({
  state: {
    productlist,
    countProducts: 0,
    total: 0
  },
  mutations: {
    addToCart(state, product) {
      let isExistProduct = false;
      state.productlist.forEach(item => {
        if (item.id === product.id) {
          item.count++;
          isExistProduct = true;
        }
      });
      if (!isExistProduct) { // Если товара нет в корзине, то добавляем
        product.count = 1; // Устанавливаем количество единиц товара
        product.date = new Date(); // Дата добавления товара
        state.productlist.push(product);
      }
      localStorage.setItem("productlist", JSON.stringify(state.productlist));
    },
    // Обновить сумму заказа и заодно обновляем количесто товара в корзине
    updatePrice(state) {
      state.total = 0;
      state.countProducts = 0;
      state.productlist.forEach(item => {
        state.total += item.price * item.count;
        state.countProducts += item.count;
      });
    },
    deleteProduct(state, id) {
      state.productlist = state.productlist.filter(item => item.id != id);
      localStorage.setItem("productlist", JSON.stringify(state.productlist));
    },
    plusCount(state, id) {
      state.productlist.forEach(item => {
        if (item.id === id) {
          item.count++;
          localStorage.setItem("productlist", JSON.stringify(state.productlist));
        }
      });
    },
    minusCount(state, id) {
      state.productlist.forEach(item => {
        if (item.id === id) {
          item.count--;
          localStorage.setItem("productlist", JSON.stringify(state.productlist));
        }
      });
    },
    deleteAll(state) {
      state.productlist = [];
      localStorage.setItem("productlist", JSON.stringify([]));
    }
  },
  actions: {
    addToCart(store, product) {
      store.commit("addToCart", product);
      store.commit("updatePrice");
    },
    updatePrice(store) {
      store.commit("updatePrice");
    },
    plusCount(store, id) {
      store.commit("plusCount", id);
      store.commit("updatePrice");
    },
    minusCount(store, id) {
      store.commit("minusCount", id);
      store.commit("updatePrice");
    },
    deleteProduct(store, id) {
      store.commit("deleteProduct", id);
      store.commit("updatePrice");
    },
    deleteAll(store) {
      store.commit("deleteAll");
      store.commit("updatePrice");
    }
  }
});
